﻿using UnityEngine;
using System.Collections;

public class AnyKeyLoadScene : MonoBehaviour {

	public string sceneToLoad = "Menu";

	// Update is called once per frame
	void Update () 
	{
		if (Input.anyKey)
        {
            Application.LoadLevel(sceneToLoad);
        }
	}
}
