﻿using UnityEngine;
using System.Collections;
using CapoeiraGames.SmashBattle.Scripts.Inputs;

public class KeyboardInput : BaseInput
{

	public bool Jump ()
	{
		return Input.GetButtonDown ("Jump");
	}

	public bool Crouch ()
	{
		return Input.GetKey (KeyCode.LeftControl);
	}

	public Vector2 Move ()
	{
		return new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
	}

	public bool Shift ()
	{
		return Input.GetKeyDown (KeyCode.LeftShift);
	}

	public bool Pause () {
		return Input.GetButtonDown("Escape");
	}
}
