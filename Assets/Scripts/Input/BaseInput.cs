﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CapoeiraGames.SmashBattle.Scripts.Inputs
{
    public interface BaseInput
    {
        bool Jump();

        bool Crouch();

        Vector2 Move();

		bool Shift();

		bool Pause();
    }
}
