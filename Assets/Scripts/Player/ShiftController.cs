﻿using UnityEngine;
using System.Collections;


public class ShiftController : MonoBehaviour
{
	[SerializeField] 
	private int currentColor = 10;

	[SerializeField]
	int playerLayer = 11;

	[SerializeField]
	LayerMask levelLayer = 1 << 9;

	const int BLACK_LAYER = 10;
	const int WHITE_LAYER = 9;

	Transform groundCheck;								// A position marking where to check if the player is grounded.
	float groundedRadius = .5f;							// Radius of the overlap circle to determine if grounded

	private bool onSwitch = false;

	Rigidbody2D rbody2D;

	public Vector2 size = new Vector2(0.72f, 2.4f);

	void Awake()
	{
		currentColor = BLACK_LAYER;
		TurnBlack();
		rbody2D = rigidbody2D;
		rbody2D.WakeUp();
		groundCheck = transform.Find("CeilingCheck");

	}

	public PlayerColor ReturnCurrentColor()
	{
		if(currentColor == BLACK_LAYER)
			return PlayerColor.Black;
		else
			return PlayerColor.White;
	}

	public void TurnBlack()
	{
		currentColor = BLACK_LAYER;
		Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,true);
		Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,false);
	}

	public void TurnWhite()
	{
		Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,false);
		Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,true);
	}
	
	//Return the crurrent color layer, not just the color for upwards use.
	public int CircleColorAndLayer()
	{
		onSwitch = true;
		
		Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,false);
		Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,false);	
//		Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,true);
//		Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,true);
		collider2D.isTrigger = true;
		switch (currentColor) {
		case BLACK_LAYER:
			currentColor = WHITE_LAYER;
			//TurnWhite ();
			break;
		case WHITE_LAYER:
			currentColor = BLACK_LAYER;
			
			//TurnBlack ();
			//rigidbody2D.AddForce(new Vector2(0f,0f));
			break;
		}

		return 1 << currentColor;
	}

	void FixedUpdate()
	{
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, Mathf.Infinity,levelLayer);
		
		if(hit.collider != null)
		{
//			Debug.Log ("Target Position: " + hit.collider.gameObject.name + " cc:" + currentColor);
			if (hit.collider.gameObject.layer != currentColor) {
	//			Debug.Log("Switch off");
//				onSwitch = false;

			}
		}
//		Collider2D coll = Physics2D.OverlapCircle(transform.position, gizmos, levelLayer);
//		if (coll)
//		{
//			Debug.Log("Fantastic!: " + coll.name + " white: " + coll.gameObject.layer + " Current: " + currentColor);
//
//			if (coll.gameObject.layer == currentColor) {
//				Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,true);
//				Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,true);	
//				Debug.Log("fdsaIgnore: ");
//
//			} else {
//				 if (Physics2D.GetIgnoreCollision(collider2D, coll)) {
//					Debug.Log("Ignore: ");
//				}
//				Physics2D.IgnoreLayerCollision(playerLayer,currentColor,true);
//
//			}
//		}


//		Vector2 position = new Vector2(transform.position.x - size.x, transform.position.y - size.y);
//		RaycastHit2D hit2D = Physics2D.BoxCast(position, size, 0.0f, Vector2.right,
//		                  levelLayer);
//
//		Collider2D coll = hit2D.collider;
//		if(coll && onSwitch) 
//		{
//			coll.isTrigger = true;
//		}

	}

	void OnCollisionStay2D(Collision2D collisionInfo) 
	{
//		if (onSwitch)
//		{
//			Debug.Log("Teste: " + collisionInfo.collider.name + " Sleeping? " + rbody2D.IsSleeping());
//			rbody2D.WakeUp();
//
//			collisionInfo.collider.isTrigger = true;				
//		}
//		Debug.Log(collisionInfo.gameObject.name);
//		if (collisionInfo.gameObject.layer != currentColor)
//		{
//			if (!onSwitch) {
//				collider2D.isTrigger = true;
//				onSwitch = true;
//			}
//		}
	}
//	
	void OnTriggerStay2D(Collider2D other) 
	{
//		if (other.gameObject.layer == currentColor) {
//			Debug.Log("Switch off");
//			onSwitch = false;
//		}
	}
	
	void OnTriggerExit2D(Collider2D other) 
	{
		Debug.Log("Trigger: " + (other.gameObject.layer != currentColor));
//		Collider2D coll = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, levelLayer);
//		if (coll.gameObject != other) {
//		}
		if (other.tag == "Level")
		{
//			if (other.gameObject.layer == currentColor) {
//			x	Debug.Log("Switch off");

			onSwitch = false;
			//			}
			Debug.Log(other.gameObject.layer);
			collider2D.isTrigger = false;
			//			other.isTrigger = false;
			switch (currentColor) {
			case BLACK_LAYER:
				Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,false);
				Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,true);
				//TurnWhite ();
				break;
			case WHITE_LAYER:
				Physics2D.IgnoreLayerCollision(playerLayer,BLACK_LAYER,true);
				Physics2D.IgnoreLayerCollision(playerLayer,WHITE_LAYER,false);
				break;
			}
		}
	}

	public float gizmos = 1.2f;
	void OnDrawGizmos()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawWireCube(transform.position, size);

	}
}


public enum PlayerColor
{
	White,
	Black,
}
