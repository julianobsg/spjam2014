﻿using UnityEngine;
using System.Collections;

public class TeleportPad : MonoBehaviour {
	public Transform teleportTarget;
	public PlayerColor padColor = PlayerColor.White;
	public GameObject teleportEffect;

	private Transform player;

	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("Teleport trigger");
		if(other.tag == "Player")
		{
			PlayerColor color = other.GetComponent<ShiftController>().ReturnCurrentColor();
			if(color == padColor)
			{
				player = other.transform;
				Debug.Log("Teleporting...");
				if(teleportEffect)
				{
					Instantiate(teleportEffect, this.transform.position, Quaternion.identity);
					Instantiate(teleportEffect, teleportTarget.position, Quaternion.identity);
				}
				player.position = teleportTarget.position;
			}
		}
	}
}
