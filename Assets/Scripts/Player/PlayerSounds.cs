﻿using UnityEngine;
using System.Collections;

public class PlayerSounds : MonoBehaviour {

	public AudioClip[] sounds;
	
	/// <summary>
	/// Plays the Pixel sounds
	/// </summary>
	/// <param name='indSound'>
	/// Sound index:
	/// 0. Step Concrete
	/// 1. Step Snow
	/// 2. Step Wing
	/// 3. Steps
	/// 4. 
	/// </param>
	public void PlaySound ( int indSound ) {
		audio.clip = sounds[indSound];
		if ( !audio.isPlaying )
			audio.Play();
	}
	
	public float ClipLenght(int idSound){
		return sounds[idSound].length;
	}
}
