﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	tk2dCamera cam;
	public Transform target;
	public float followSpeed = 1.0f;
	
	public float minZoomSpeed = 20.0f;
	public float maxZoomSpeed = 40.0f;
	
	public float maxZoomFactor = 0.6f;

	[SerializeField]
	private Vector3 centerPosition;

	private float actualDirection = 1.0f;
	
	public CameraBoundaries boundaries;

	void Awake() 
	{
		cam = GetComponent<tk2dCamera>();

	}
	
	void FixedUpdate() 
	{
		Vector3 start = transform.position;

		Vector3 cameraCenter = centerPosition;
		cameraCenter.x *= actualDirection;

		if (target.rigidbody2D != null && cam != null) 
		{
			float spd = target.rigidbody2D.velocity.magnitude;
			Vector2 direction =  target.rigidbody2D.velocity.normalized;

			if (direction.x != 0) {
				actualDirection = direction.x;
			}
			float scl = Mathf.Clamp01((spd - minZoomSpeed) / (maxZoomSpeed - minZoomSpeed));

			float targetZoomFactor = Mathf.Lerp(1, maxZoomFactor, scl);

			cam.ZoomFactor = Mathf.MoveTowards(cam.ZoomFactor, targetZoomFactor, 0.2f * Time.deltaTime);
		}

		Vector2 cameraTarget = target.position + cameraCenter;

//		Vector2 clampedTarget = new Vector2 (Mathf.Clamp(cameraTarget.x, boundaries.left / 2, boundaries.right / 2),
//						 		Mathf.Clamp(cameraTarget.y, boundaries.bottom / 2, boundaries.top / 2));
		Vector3 end = Vector3.MoveTowards(start, cameraTarget, followSpeed * Time.deltaTime);
		end.z = start.z;
		transform.position = end;
	}

	void OnDrawGizmosSelected ()
	{
		Gizmos.color = Color.red;
		Vector2 topLeft = new Vector2(boundaries.left, boundaries.top);
		Vector2 topRight = new Vector2(boundaries.right, boundaries.top);
		Vector2 bottomRight = new Vector2(boundaries.right, boundaries.bottom);
		Vector2 bottomLeft = new Vector2(boundaries.left, boundaries.bottom);

		Gizmos.DrawLine(topLeft, topRight);
		Gizmos.DrawLine(topLeft, bottomLeft);
		Gizmos.DrawLine(topRight, bottomRight);
		Gizmos.DrawLine(bottomLeft, bottomRight);

	}
}

[System.Serializable]
public class CameraBoundaries
{
	public float top;
	public float left;
	public float right;
	public float bottom;
}