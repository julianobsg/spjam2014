﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour
{
	Animator anim;										// Reference to the player's animator component.
	
	void Awake()
	{
		anim = GetComponent<Animator>();
	}

	public void Grounded (bool grounded)
	{
		anim.SetBool("Ground", grounded);
	}

	public void Crouched (bool crouched)
	{
//		anim.SetBool("Crouch", crouched);
	}

	public void VerticalSpeed (float ySpeed)
	{
		anim.SetFloat("vSpeed", ySpeed);
	}

	public void HorizontalSpeed(float xSpeed)
	{
		anim.SetFloat("Speed", xSpeed);

	}

	public void Shift ()
	{
		anim.SetBool("Shifting", true);
	}

	public void ShiftOver ()
	{
		anim.SetBool("Shifting", false);
	}

	public void Damage()
	{
		anim.SetBool("Damage", true);
	}

	public void DamageOver()
	{
		anim.SetBool("Damage", false);
	}

	public void Revive()
	{
		anim.SetBool("Revive", true);
	}

	public void ReviveOver()
	{
		anim.SetBool("Revive", false);
	}
}
