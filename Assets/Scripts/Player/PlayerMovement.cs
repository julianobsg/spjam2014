﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour 
{
	[SerializeField] bool facingRight = true;			// For determining which way the player is currently facing.

	[SerializeField] float maxSpeed = 10f;				// The fastest the player can travel in the x axis.
	[SerializeField] float jumpForce = 400f;			// Amount of force added when the player jumps.	

	[SerializeField] bool airControl = false;			// Whether or not a player can steer while jumping;
	[SerializeField] LayerMask whatIsGround;			// A mask determining what is ground to the character
	
	Transform groundCheck;								// A position marking where to check if the player is grounded.
	float groundedRadius = .2f;							// Radius of the overlap circle to determine if grounded
	bool grounded = false;								// Whether or not the player is grounded.

	public bool IsGrounded
	{
		get { return grounded; }
	}

	public LayerMask Grounded 
	{
		get{return whatIsGround;}
		set{whatIsGround = value;}
	}	

	Transform ceilingCheck;								// A position marking where to check for ceilings
	float ceilingRadius = .01f;							// Radius of the overlap circle to determine if the player can stand up

	private Rigidbody2D myRigidbody2D;
	private Transform myTransform;

	void Awake()
	{
		myRigidbody2D = rigidbody2D;
		myTransform = transform;

		groundCheck = myTransform.Find("GroundCheck");
		ceilingCheck = myTransform.Find("CeilingCheck");
	}


	void FixedUpdate()
	{
		// The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundedRadius, whatIsGround);
	}


	public void Move(float move, bool crouch, bool jump)
	{
		// If crouching, check to see if the character can stand up
		if(!crouch)
		{
			
			// If the character has a ceiling preventing them from standing up, keep them crouching
			if( Physics2D.OverlapCircle(ceilingCheck.position, ceilingRadius, whatIsGround))
				crouch = true;

			if(grounded || airControl)
			{
				myRigidbody2D.velocity = new Vector2(move * maxSpeed, myRigidbody2D.velocity.y);
			
				if(move > 0 && !facingRight)
					Flip();
				else if(move < 0 && facingRight)
					Flip();
			}
		}


		if (grounded && jump) {
			myRigidbody2D.velocity = new Vector2(0f, jumpForce);
		}
	 }


	void Flip()
	{
		facingRight = !facingRight;

		Vector3 theScale = myTransform.localScale;
		theScale.x *= -1;
		myTransform.localScale = theScale;
	}

	public void ResetXVelocity()
	{
		myRigidbody2D.velocity = new Vector2(0, myRigidbody2D.velocity.y);
	}
}
