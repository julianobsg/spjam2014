﻿using UnityEngine;
using System.Collections;
using CapoeiraGames.SmashBattle.Scripts.Inputs;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(ShiftController))]

public class PlayerController : MonoBehaviour 
{
	private PlayerMovement playerMovement;
	private PlayerAnimation playerAnimation;
	private PlayerSounds ps;
	private ShiftController shiftController;
	//private bool pause;
	private bool jump;
	private bool canMove = true; //used for player Deaths disable movments
	private bool shift;

	public BaseInput input;

	//public GameObject pauseMenu;
	//Death Variables
	public string deathTag = "Hazard"; //used for player deaths, hazardous objects identifier
	public Transform spawnPoint; //Place to respawn
	public string checkPointTag = "CheckPoint"; //CheckPoint identifier
	public float deadTime = 1F; //Time Rendered Dead

	public LayerMask groundMask;

	void Awake()
	{
		input = new KeyboardInput();
		playerMovement = GetComponent<PlayerMovement>();
		playerAnimation = GetComponent<PlayerAnimation>();
		ps = GameObject.FindWithTag("MainCamera").GetComponent<PlayerSounds>();
		shiftController = GetComponent<ShiftController>();
	}

	void Update ()
	{
		/*
		if (input.Pause()){
			pause = !pause;
			if(pause){
				Time.timeScale = 0;
				if(pauseMenu){
					pauseMenu.transform.position = transform.position;
					pauseMenu.SetActive(true);
				}
			}
			else{
				Time.timeScale = 1;
				if(pauseMenu){
					Vector3 outofsight = new Vector3(0,0,-30);
					pauseMenu.transform.position = outofsight;
					pauseMenu.SetActive(false);
				}
			}
		}
		*/
		if(input.Jump())
			jump = true;
		shift = input.Shift();

		if(shift)
		{
			groundMask = shiftController.CircleColorAndLayer();
			playerAnimation.Shift();
			playerMovement.Grounded = groundMask;
		}
	}

	void FixedUpdate()
	{
		bool crouch = input.Crouch();

		Vector2 steer = input.Move();

		float x = steer.x;
		if (canMove){
			if (playerAnimation) 
			{
				ManageAnimations(crouch, x);
				if (x != 0 && playerMovement.IsGrounded && ps)
					ps.PlaySound(2);
			}

			playerMovement.Move(x, crouch , jump);

			jump = false;
		}

	}

	private void ManageAnimations(bool crouch, float x)
	{
		playerAnimation.HorizontalSpeed(Mathf.Abs(x));

		playerAnimation.Grounded(playerMovement.IsGrounded);

		playerAnimation.Crouched(crouch);
	}

	//Stop PlayerMovement, wait for spawn time and spawn
	private IEnumerator Death(){
		Debug.Log("Death");
		playerAnimation.Damage();
		canMove = false;
		playerMovement.ResetXVelocity();

		yield return(new WaitForSeconds(deadTime));
		shiftController.TurnBlack();
		if(spawnPoint){
			transform.position = spawnPoint.position;
		}
		Debug.Log("Alive");
		playerAnimation.DamageOver();
		playerAnimation.Revive();
		canMove = true;
		//playerMovement.rigidbody2D.isKinematic = true; //Stop Rigidbody physics
	}

	//Death By Trigger
	void OnTriggerEnter2D(Collider2D other){
		//If Enter a Hazardous zone the player dies
		if (other.CompareTag(deathTag)){
			StartCoroutine(Death());
		//Otherwise if he hits a checkpoint, the spawn position recieves the checkpoint position.
		} else if (other.CompareTag(checkPointTag)){
			spawnPoint = other.transform;
			Debug.Log("Checkpoint!");
		}
	}
	//Death By Collision
	void OnCollisionEnter2D(Collision2D collision){
		Collider2D other = collision.collider;
		//Debug.Log("Hit on: "+ other.tag);
		if (other.CompareTag(deathTag)){
			StartCoroutine(Death());
		}
	}
	
}
