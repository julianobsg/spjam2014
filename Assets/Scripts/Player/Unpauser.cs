﻿using UnityEngine;
using System.Collections;

public class Unpauser : MonoBehaviour {
	public bool reload = false;

	private Pauser myPause;
	void Awake (){
		myPause = GameObject.FindWithTag("MainCamera").GetComponent<Pauser>();
	}

	void OnMouseUp(){

		if(myPause){
			myPause.UnpauseGame();
		}
		if(reload){
			Application.LoadLevel(Application.loadedLevel);
		}

	}
}
