﻿using UnityEngine;
using System.Collections;

public class PlayerShifter : MonoBehaviour {

	public PlayerLayer startLayer = PlayerLayer.PlayerBlack;
	private PlayerLayer currentPlayerLayer;
	private GameObject myGameObject;

	// Use this for initialization
	void Awake () {
		myGameObject = this.gameObject;
		currentPlayerLayer = startLayer;
		myGameObject.layer = (int)currentPlayerLayer;
	}

	// Update is called once per frame
	void Update () {
		
	}

	//Effectively shifts the player layer between 
	//PlayerBlack and PlayerWhite
	public void ChangeColor(){
		switch (currentPlayerLayer) {
		//if it is black, go white
		case PlayerLayer.PlayerBlack:
			//change the layer;
			myGameObject.layer = (int)PlayerLayer.PlayerWhite;
			//changes the current layer flag
			currentPlayerLayer = PlayerLayer.PlayerWhite;
			break;
		//if it is white, go black
		case PlayerLayer.PlayerWhite:
			myGameObject.layer = (int)PlayerLayer.PlayerBlack;
			//changes the current layer flag
			currentPlayerLayer = PlayerLayer.PlayerBlack;
			break;
		}
		myGameObject.collider2D.isTrigger = true;
		//ResolveBadCollision ();
	}
	//used to correct the faulty behaviour when the player shifts layer
	//inside the same collor.
	//used with on trigger exit.
	private void ResolveBadCollision()
	{
		if(!Physics2D.GetIgnoreLayerCollision((int)currentPlayerLayer, myGameObject.layer)){
			//Debug.Log("Go trigger");
			myGameObject.collider2D.isTrigger = true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		//Debug.Log ("Player Exit Trigger on: " + other.name);
		if(other.CompareTag("Level")){
			myGameObject.collider2D.isTrigger = false;
		}
	}
	public int CurrentLayer
	{
		get { return ((int)currentPlayerLayer);}
	}

	public void SetLayer (PlayerLayer layerColor){
		myGameObject.layer = (int)layerColor;
		currentPlayerLayer = layerColor;
	}
}

public enum PlayerLayer {
	PlayerBlack = 11,
	PlayerWhite = 12
}
