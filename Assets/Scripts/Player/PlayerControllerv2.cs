﻿using UnityEngine;
using System.Collections;
using CapoeiraGames.SmashBattle.Scripts.Inputs;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerShifter))]

public class PlayerControllerv2 : MonoBehaviour 
{
	private PlayerMovement playerMovement;
	private PlayerAnimation playerAnimation;
	private PlayerSounds ps;
	private PlayerShifter shiftController;
	//private bool pause;
	private bool jump;
	private bool isDead = false; //used for player Deaths disable movments
	private bool shift;



	public BaseInput input;

	//public GameObject pauseMenu;
	//Death Variables
	public string deathTag = "Hazard"; //used for player deaths, hazardous objects identifier
	public Transform spawnPoint; //Place to respawn
	public string checkPointTag = "CheckPoint"; //CheckPoint identifier
	public float deadTime = 1F; //Time Rendered Dead

	public LayerMask groundMask;
	public int previousGround; // record the last ground mask

	#region MonoBehaviour
	void Awake()
	{
		input = new KeyboardInput();
		playerMovement = GetComponent<PlayerMovement>();
		playerAnimation = GetComponent<PlayerAnimation>();
		ps = GameObject.FindWithTag("MainCamera").GetComponent<PlayerSounds>();
		shiftController = GetComponent<PlayerShifter>();

	}

	void Update ()
	{
		/*
		if (input.Pause()){
			pause = !pause;
			if(pause){
				Time.timeScale = 0;
				if(pauseMenu){
					pauseMenu.transform.position = transform.position;
					pauseMenu.SetActive(true);
				}
			}
			else{
				Time.timeScale = 1;
				if(pauseMenu){
					Vector3 outofsight = new Vector3(0,0,-30);
					pauseMenu.transform.position = outofsight;
					pauseMenu.SetActive(false);
				}
			}
		}
		*/
		if (!isDead) {
			if (input.Jump ())
				jump = true;

			shift = input.Shift ();

			if (shift) {
				//groundMask = shiftController.CircleColorAndLayer();
				//ShiftGround ();
				//Debug.Log(groundMask);
				previousGround = groundMask;
				setGround(13);
				shiftController.ChangeColor ();
				playerAnimation.Shift ();
				//playerMovement.Grounded = groundMask;
			//Debug.Log(playerMovement.Grounded);
			}

		}
	}

	void FixedUpdate()
	{
		bool crouch = input.Crouch();

		Vector2 steer = input.Move();

		float x = steer.x;
		if (!isDead){
			if (playerAnimation) 
			{
				ManageAnimations(crouch, x);
				if (x != 0 && playerMovement.IsGrounded && ps)
					ps.PlaySound(2);
			}

			playerMovement.Move(x, crouch , jump);

			jump = false;
		}

	}
	#endregion

	private void ManageAnimations(bool crouch, float x)
	{
		playerAnimation.HorizontalSpeed(Mathf.Abs(x));

		playerAnimation.Grounded(playerMovement.IsGrounded);

		playerAnimation.Crouched(crouch);
	}


	
	//used to change what is suposed to be considered ground
	private void ShiftGround(){
		//Debug.Log ("shiftground" + groundMask.ToString());
		switch (previousGround) {
		case 1<<10: //black layer index
			groundMask = 1 << 9;//white layer
			//Debug.Log("changing ground layer to: "+groundMask);
			break;
		case 1<<9: //white layer index
			groundMask = 1<<10;//black layer
			//Debug.Log("changing ground layer to: "+groundMask);
			break;
		}
		//Debug.Log ("mimimi");
		playerMovement.Grounded = groundMask;
	}
	//sets a forced groundMask, based on the layer index
	//9 = White
	//10 = Black
	private void setGround(int layerIndex)
	{
		groundMask = 1 << layerIndex;
		playerMovement.Grounded = groundMask;
	}
	//i did the one bellow because of reasons
	private void SetGround(params int[] layerIndexes){
		for (int i = 0; i < layerIndexes.Length; i++) 
		{
			groundMask |= 1 << layerIndexes[i];

		}
		playerMovement.Grounded = groundMask;
	}
	#region Death
	//Death By Trigger
	void OnTriggerEnter2D(Collider2D other){
		//If Enter a Hazardous zone the player dies
		if (other.CompareTag(deathTag) && !isDead){
			Debug.Log("Death by trigger with: " + other.name);
			StartCoroutine(Death());
		//Otherwise if he hits a checkpoint, the spawn position recieves the checkpoint position.
		} else if (other.CompareTag(checkPointTag)){
			spawnPoint = other.transform;
			//Debug.Log("Checkpoint!");
		} 
	}
	void OnTriggerExit2D(Collider2D other){
		if(other.CompareTag("Level")){
			//Debug.Log("change ground");
			ShiftGround();
		}
	}
	//Death By Collision
	void OnCollisionEnter2D(Collision2D collision){
		Collider2D other = collision.collider;

		//Debug.Log("Hit on: "+ other.tag);
		if (other.CompareTag(deathTag) && !isDead){
			Debug.Log("Death by collison with: " + other.name);
			StartCoroutine(Death());
		}
	}

	//Stop PlayerMovement, wait for spawn time and spawn
	private IEnumerator Death(){
		//Debug.Log("Death");
		playerAnimation.Damage();
		isDead = true;
		playerMovement.ResetXVelocity();
		yield return(new WaitForSeconds(deadTime));
		if(spawnPoint){
			transform.position = spawnPoint.position;
		}

		playerAnimation.DamageOver();
		//Debug.Log("Alive");
		Debug.Log ("Change ur fucking layer asshole");
		shiftController.SetLayer(PlayerLayer.PlayerBlack);
		setGround (10);
		yield return(new WaitForSeconds(deadTime));
		playerAnimation.Revive();
		isDead = false;
		//playerMovement.rigidbody2D.isKinematic = true; //Stop Rigidbody physics
	}
	#endregion
	
}
