﻿using UnityEngine;
using System.Collections;
using CapoeiraGames.SmashBattle.Scripts.Inputs;

public class Pauser : MonoBehaviour {

	public GameObject pauseMenu;
	private BaseInput input;
	private Transform myCamera;
	private bool pause;
		
	// Use this for initialization
	void Awake () {
		myCamera = GameObject.FindWithTag("MainCamera").transform;
		input = new KeyboardInput();
	}
	
	// Update is called once per frame
	void Update () {
		if (input.Pause()){
			pause = !pause;
			if(pause){

				PauseGame();
			}
			else{
				UnpauseGame();
			}
		}
	}

	public void PauseGame(){
		Time.timeScale = 0;
		if(pauseMenu){
			Vector3 menuPos;
			menuPos = myCamera.position;
			menuPos.z = -0.5f;
			pauseMenu.transform.position = menuPos;
			pauseMenu.SetActive(true);
		}else
			Debug.LogWarning("No Pause Menu");
	}
	public void UnpauseGame(){
		Time.timeScale = 1;
		if(pauseMenu){
			Vector3 outofsight = new Vector3(0,0,-80);
			pauseMenu.transform.position = outofsight;
			pauseMenu.SetActive(false);
			pause = false;
		}
	}

}
