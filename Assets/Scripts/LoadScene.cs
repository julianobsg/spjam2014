﻿using UnityEngine;
using System.Collections;

public class LoadScene : MonoBehaviour {

	public string sceneName; 
	

	void OnMouseUp(){
		if (!string.IsNullOrEmpty(sceneName)){
			switch (sceneName){
			case "Exit":
				Application.Quit();
				break;
			case "Reload":
				Application.LoadLevel(Application.loadedLevel);
				break;
			default:
				Application.LoadLevel(sceneName);
				break;

			}

		}
	}
}
