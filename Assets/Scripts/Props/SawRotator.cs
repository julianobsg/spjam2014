﻿using UnityEngine;
using System.Collections;

public class SawRotator : MonoBehaviour {

	public float rotationSpeed = 90;

	private Transform myTransform;

	void Start()
	{
		myTransform = this.transform;
	}

	// Update is called once per frame
	void Update () 
	{
		myTransform.Rotate(new Vector3(0,0,1)*rotationSpeed*Time.deltaTime);
	}
}
