﻿using UnityEngine;
using System.Collections;

public class NextLevelPortal : MonoBehaviour {

	public string nextLevelName;
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.tag == "Player")
		{
			Application.LoadLevel(nextLevelName);
		}
	}
}
