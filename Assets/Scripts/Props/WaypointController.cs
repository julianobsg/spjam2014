﻿using UnityEngine;
using System.Collections;

public class WaypointController : MonoBehaviour {
	
	public enum wpType { Once, Loop, PingPong }

	public wpType type = wpType.Loop;

	public GameObject[] waypoints;
	public float speed = 3;
	public int index = 0;
	
	private GameObject nextWP;
	private float timer = 0;
	private int step = 1;
	public float WPMinDistance = 0.2f;
	
	void Start() {
		if ( nextWP == null && waypoints.Length > 0 ) nextWP = waypoints[index];
	}

	void Update () {
		if ( timer <= 0 && nextWP != null ) {
			Vector3 direction = (nextWP.transform.position - transform.position);
			direction.Normalize();
			transform.Translate( direction * Time.deltaTime * speed, Space.Self );
		} else {
			timer -= Time.deltaTime;
		}
		
		// If I arrive on the next WayPoint
		if ( Vector3.Distance( transform.position, nextWP.transform.position ) <= WPMinDistance ) WPArrive();
		
	}
	
	void WPArrive () {
		index += step;
		timer = nextWP.GetComponent<Waypoint>().delay;
		
		if ( type == wpType.Loop && index >= waypoints.Length ) {
			index = 0;
		}

		if ( type == wpType.PingPong ) {
			if ( index >= waypoints.Length ) {
				index = waypoints.Length - 2;
				step = step * (-1);
			}
			if ( index < 0 ) {
				index = 1;
				step = step * (-1);
			}
		}
		
		if ( type == wpType.Once && index >= waypoints.Length ) {
			nextWP = null;
			return;
		}
		
		nextWP = waypoints[index];
	}

	void OnDrawGizmos () {
		for ( int i = 0; i < waypoints.Length; i++ ) {
			Gizmos.color = Color.cyan;
			Gizmos.DrawLine( waypoints[i].transform.position, transform.position );
			Gizmos.color = Color.white;
			if ( i+1 < waypoints.Length )
				Gizmos.DrawLine( waypoints[i].transform.position, waypoints[i+1].transform.position );
		}
		if ( type == wpType.Loop ) Gizmos.DrawLine( waypoints[0].transform.position, waypoints[waypoints.Length-1].transform.position );
	
		
	}
	
}