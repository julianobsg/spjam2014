﻿using UnityEngine;
using System.Collections;

public class DartTrap : MonoBehaviour {
	public GameObject dartPrefab;
	public float timerToShoot = 5f;
	public Vector2 dartMovingVector = Vector2.right;
	public float dartLifeTime = 1f;
	public bool useTrapRotation = false;
	public float trapRotationSpeed = 1f;

	private Transform myTransform;

	// Use this for initialization
	void Start () {
		myTransform = transform;
		StartCoroutine(Shoot ());
		if(useTrapRotation)
			StartCoroutine(DiscreteRotation());
	}

	IEnumerator DiscreteRotation()
	{
		yield return new WaitForSeconds(trapRotationSpeed);
		myTransform.Rotate(new Vector3(0,0,90));
		StartCoroutine(DiscreteRotation());
	}

	IEnumerator Shoot()
	{
		yield return new WaitForSeconds(timerToShoot);
		Vector3 pos = this.transform.position;
		pos -= new Vector3(0,0, -0.25f);
		GameObject dart = Instantiate(dartPrefab, pos, Quaternion.identity) as GameObject;
		dart.GetComponent<DartMover>().SetDart(dartMovingVector, this.transform.rotation, dartLifeTime);
		StartCoroutine(Shoot ());
	}


}
