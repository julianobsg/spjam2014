﻿using UnityEngine;
using System.Collections;

public class DartMover : MonoBehaviour {

	private float lifeTime = 1f;
	private Vector2 speedVector;
	private Transform myTransform;

	void Awake()
	{
		myTransform = this.transform;
		speedVector = myTransform.right;
	}

	void Update () 
	{
		myTransform.Translate(speedVector*Time.deltaTime);
	}

	public void SetDart(Vector2 shootDirection, Quaternion trapRotation, float lifeTime)
	{
		speedVector = shootDirection;
		myTransform.rotation = trapRotation;
		this.lifeTime = lifeTime;
		StartCoroutine(KillTimer());
	}

	IEnumerator KillTimer()
	{
		yield return new WaitForSeconds(lifeTime);
		Destroy(this.gameObject);
	}

	void OnTriggerEnter2D(Collider2D other){
		//Debug.Log("dart collided with:" + other.name);
		Destroy (gameObject);
	}

}


