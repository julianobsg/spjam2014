﻿using UnityEngine;
using System.Collections;

public class SimpleUV : MonoBehaviour {
	
	public Color materialColor = Color.white;
	public bool getParentSize = false;
	public Vector2 UVRepeats = new Vector2(1,1);
	public Vector2 UVOffset = Vector2.zero;
	
	void Start () {
		if (getParentSize) GetSize ();
		setUV();
	}
	
	private void setUV() {
		Material mat = new Material(gameObject.renderer.material);
			mat.SetTextureScale("_MainTex", UVRepeats);
			mat.SetTextureOffset("_MainTex", UVOffset);
			mat.SetColor("_Color", materialColor);
		gameObject.renderer.material = mat;
	}

	private void GetSize () {
		UVRepeats = new Vector2 (transform.localScale.x, transform.localScale.y);
	}
}
